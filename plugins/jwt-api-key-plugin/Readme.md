jwt-api-key-plugin
==================

Kong plugin to retrieve a valid JWT based on provided apikey and then redirect request to its
original endpoint.

## Security notes

This plugin doesn't check the jwt validity. So if you provide `Authorization: Bearer jwt` in the incoming calls the plugin
will not validate the jwt, instead will forward the request to the API behind kong. So the final API receiving the jwt
is the responsible to check for the token validity.

## Reducing the scope of the apikeys to each API

You can configure a determinated property to have a value in order to match this field from JWT decoded payload.

So given the next JWT payload:

```json
{
  "exp": 1499690040,
  "aud": "fees",
  "username": "xxxxx@cirici.com",
  "roles": [
    "ROLE_XXXXX",
    "ROLE_XXX",
    "ROLE_XXXX"
  ],
  "permissions": [
    "service_x.can_list",
    "esrvice_y.can_remove",
  ],
  "accounts": [
    "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
  ],
  "defaultAccount": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
  "user_id": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
  "locale": "ca",
  "iat": "1499689840"
}
```

You can force the requested apikeys for a specific API to have a field (given the example `aud`) to match a value (`fees`).
So each apikey must be used only in one API.

## References

https://getkong.org/docs/0.8.x/plugin-development/

